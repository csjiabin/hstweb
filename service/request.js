import axios from 'axios'
// import router from '@router'
import Message from '../components/Message'
// axios.options.headers = {'content-type': 'application/json;charset=UTF-8'}
// axios.defaults.timeout = 180000
// import VueRouter from 'vue-router'
// const router = new VueRouter({})
// router.beforeEach((to, from, next) => {
//   console.log(to.path)
// })
const request = axios.create({ timeout: 20000 })
request.interceptors.response.use(
  response => response,
  error => {
    if (error.response) {
      switch (error.response.status) {
        case 401:
          Message.error('登录过期,请重新登录')
          localStorage.removeItem('user')
          // if (router.currentRoute.fullPath.indexOf('redirect') !== -1) return // 处理多次请求，redirect重复问题
          // router.replace({
          //   path: '/login',
          //   query: {
          //     redirect: router.currentRoute.fullPath
          //   }
          // })
          break
        case 500:
          Message.error('系统异常，请联系管理员...')
          break
        case 502:
          Message.error('系统正在重启，请稍后再试...')
          break
        case 503:
          Message.error('服务暂时不可用...')
          break
        case 504:
          Message.error('系统繁忙，请稍后再试...')
          break
      }
      return Promise.reject(
        error.response
          ? Object.assign(error.response.data, {
              status: error.response.status
            })
          : { message: '页面请求失败！' }
      ) // 返回接口返回的错误信息
    }
  }
)

export default request
