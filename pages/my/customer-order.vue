<template>
  <div class="customer-order">
    <div class="page-container">
      <div
        :style="{
          width: '1000px',
          margin: 'auto'
      }">
        <v-card v-loading="loading">
          <search-box
            width="420px"
            placeholder="输入订单号查询"
            @search="searchHandle"/>
          <v-tabs
            v-model="search.status"
            grow
            class="status-tabs"
            active-class="tab-active">
            <v-tabs-slider
              color="primary"
              class="tabs-slider"/>
            <v-tab href="#1">待发件 {{ orders.pendingSendNum }}</v-tab>
            <v-tab href="#2">运输中 {{ orders.intransitNum }}</v-tab>
            <v-tab href="#3">已到仓 {{ orders.arrive }}</v-tab>
          </v-tabs>
          <div
            class="order-content">
            <template v-if="dataCount>0">
              <customer-order-item
                v-for="item in orders.list"
                :key="item.id"
                :detail="item"
                @detail-handle="detailHandle"/>
            </template>
            <v-alert
              :value="dataCount<=0"
              outline
              color="warning"
              icon="warning">
              这里没有你想要的数据 :(
            </v-alert>

            <div class="layout align-center justify-center">
              <span
                :style="{
                  position:'absolute',
                  left:'30px',
                  color:'#8492a6',
                  fontSize:'12px'
              }">
                共{{ dataCount }}条数据
              </span>
              <v-pagination
                v-model="search.pageIndex"
                :length="pageTotal"/>
            </div>
          </div>
        </v-card>
      </div>
    </div>
    <foote/>
    <order-detail-dialog ref="dialog"/>
  </div>
</template>
<script>
import Foote from '~/components/Foote'
import SearchBox from '~/components/SearchBox'
import axios from 'axios'
import CustomerOrderItem from '~/components/page/my/CustomerOrderItem'
import OrderDetailDialog from '~/components/page/my/OrderDetailDialog'
import initMixin from '~/mixin/init-mixin.js'
import scrollIntoView from '~/utils/scroll-into-view.js'

export default {
  components: {
    Foote,
    SearchBox,
    CustomerOrderItem,
    OrderDetailDialog
  },
  mixins: [initMixin],
  layout: 'base',
  head: {
    title: '网点订单'
  },
  data() {
    return {
      search: {
        pageIndex: 1,
        pageSize: 10,
        status: '1',
        orderId: null
      },
      tab: null,
      items: [
        {
          text: '全部网点',
          value: null
        }
      ],
      orderData: {
        list: [],
        count: 0
      },
      loading: false,
      timer: null
    }
  },
  computed: {
    orders() {
      return this.$store.getters['customer/orders'] || { list: [] }
    },
    dataCount() {
      let { orders, search = { status: '0' } } = this
      const status = search.status * 1
      if (status === 0) return orders.pendingReceiveNum || 0
      if (status === 1) return orders.pendingSendNum || 0
      if (status === 2) return orders.intransitNum || 0
      return orders.arrive || 0
    },
    pageTotal() {
      const count = this.dataCount
      return Math.ceil(count / this.search.pageSize)
    }
  },
  watch: {
    search: {
      handler() {
        clearTimeout(this.timer)
        this.timer = setTimeout(async () => {
          await this.getOrderList()
        }, 300)
      },
      deep: true
    },
    'search.status'() {
      this.search.pageIndex = 1
      this.search.pageSize = 10
      this.$nextTick(() =>
        scrollIntoView(document.documentElement || document.body)
      )
    }
  },
  async mounted() {
    await this.getOrderList()
  },
  methods: {
    searchHandle(val) {
      this.search.orderId = val
    },
    detailHandle({ id }) {
      const dialog = this.$refs.dialog
      dialog.show(id)
    },
    async getOrderList() {
      this.loading = true
      let params = { ...this.search }
      params.pageIndex--
      params.status = params.status * 1
      await this.$store.dispatch('customer/list', params)
      this.loading = false
    }
  }
}
</script>
<style lang="stylus" scoped>
@require '~assets/style/variable.styl'
.customer-order
  .v-card
    padding-top 30px
  .search-box
    display block
    margin auto
  .status-tabs
    width 660px
    margin 12px auto 0
    >>>.tab-active
      pointer-events none
      color green-color
    .tabs-slider
      height 3px
  .order-content
    border-top solid 1px light-border-color
    padding 20px 30px
    .outlet-select
      width 180px
</style>
