module.exports = {
  '/autonumber': {
    target: 'https://www.kuaidi100.com',
    changeOrigin: true, // 开启代理
    pathRewrite: {
      '^/autonumber': '/autonumber'
    } // 这里重写路径/run就代理到对应地址
  },
  '/query': {
    target: 'https://www.kuaidi100.com',
    changeOrigin: true, // 开启代理
    pathRewrite: {
      '^/query': '/query'
    }
  },
  '/api': {
    // 这里是我配置的名字
    target: 'http://192.168.200.129:8881',
    // target: 'http://192.168.200.82:8881',
    changeOrigin: true, // 开启代理
    pathRewrite: {
      '^/api': '/api'
    }
  }
}
