/**
 * store customer
 */
import request from '~/service/request'
export const state = () => ({
  orders: {
    list: [],
    count: 0
  }
})
export const getters = {
  orders: state => state.orders || {}
}
export const actions = {
  async list({ commit }, params = {}) {
    try {
      const { data } = await request.post(
        '/api/platformOrder/customer/list',
        params
      )
      commit('SET_STATE', { data, target: 'orders' })
      return data
    } catch (error) {
      throw error
    }
  }
}
export const mutations = {
  SET_STATE(state, payload) {
    const { target, data } = payload
    state[target] = data
  }
}
