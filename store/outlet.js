/**
 * store outlet
 */
import request from '~/service/request'
export const state = () => ({
  orders: {
    list: [],
    count: 0
  },
  outlets: {
    list: [],
    count: 0
  },
  outletAll: [],
  addressList: [],
  provinces: [],
  citys: []
})
export const getters = {
  orders: state => state.orders,
  outlets: state => state.outlets,
  outletAll: state => state.outletAll,
  addressList: state => state.addressList,
  provinces: state => state.provinces,
  citys: state => state.citys
}
export const actions = {
  async getOutletOrderlist({ commit }, params = {}) {
    try {
      const { data } = await request.post(
        '/api/platformOrder/outlet/list',
        params
      )
      commit('SET_STATE', { data, target: 'orders' })
      return data
    } catch (error) {
      throw error
    }
  },
  async outletAll({ commit, state }) {
    try {
      const { data = {} } = await request.get('/api/outlet/all')
      commit('SET_STATE', {
        data: data.list || [],
        target: 'outletAll'
      })
      return data.list
    } catch (error) {
      throw error
    }
  },
  // 网点列表
  async getOutletList({ commit }, params = {}) {
    try {
      const { data } = await request.post('/api/outlet/list', params)
      commit('SET_STATE', { data, target: 'outlets' })
      return data
    } catch (error) {
      throw error
    }
  },
  // 网点地址列表
  async getOutletAddressList({ commit }) {
    try {
      const { data } = await request.get('/api/outlet/addressList')
      commit('SET_STATE', { data, target: 'addressList' })
      return data
    } catch (error) {
      throw error
    }
  },
  // 新增网点
  async addOutletLogistics({}, params = {}) {
    try {
      return await request.post('/api/outlet', params)
    } catch (error) {
      throw error
    }
  },
  // 更新网点
  async updateOutletLogistics({}, params = {}) {
    try {
      return await request.put('/api/outlet', params)
    } catch (error) {
      throw error
    }
  },
  // 全国服务网点省份
  async outletProvinceList({ commit }) {
    try {
      const { data } = await request.get('/api/index/outlet/provinceList')
      commit('SET_STATE', { data, target: 'provinces' })
      return data
    } catch (error) {}
  },
  // 全国服务网点市区查询
  async outletDetailByProvinceById({ commit }, id) {
    try {
      const { data } = await request.get(
        `/api/index/outlet/serviceOutletDetailByProvince/${id}`
      )
      commit('SET_STATE', { data, target: 'citys' })
      return data
    } catch (error) {
      throw error
    }
  }
}
export const mutations = {
  SET_STATE(state, payload) {
    const { target, data } = payload
    state[target] = data
  }
}
