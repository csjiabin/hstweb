/**
 * store order
 */
import request from '~/service/request'
export const state = () => ({})
export const getters = {}
export const actions = {
  // 统一收件
  async putPlatformOrderReceive({ commit }, params = {}) {
    try {
      return await request.put('/api/platformOrder/receive', params)
    } catch (error) {
      throw error
    }
  },
  // 统一更新
  async putPlatformOrder({}, params = {}) {
    try {
      return await request.put('/api/platformOrder', params)
    } catch (error) {
      throw error
    }
  },
  // 统一发件
  async putPlatformOrderSend({}, params = {}) {
    try {
      return await request.put('/api/platformOrder/send', params)
    } catch (error) {
      throw error
    }
  },
  async postPlatformOrderCustomer({}, params = {}) {
    try {
      return await request.post('/api/platformOrder/customer', params)
    } catch (error) {
      throw error
    }
  }
}
export const mutations = {
  SET_STATE(state, payload) {
    const { target, data } = payload
    state[target] = data
  }
}
