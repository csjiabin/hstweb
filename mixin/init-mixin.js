export default {
  data() {
    return {
      userType: 0,
      userInfo: {
        type: 0,
        user: {}
      }
    }
  },
  methods: {
    getUser() {
      let user = null
      if (process.client && !!localStorage.getItem('user')) {
        user = JSON.parse(localStorage.getItem('user'))
        this.userType = user.type || 0
      }
      return user
    }
  },
  watch: {
    $route() {
      this.userInfo = this.getUser()
    }
  },
  mounted() {
    this.userInfo = this.getUser()
  }
}
