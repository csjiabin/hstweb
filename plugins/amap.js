import Vue from 'vue'
import VueAMap from 'vue-amap'
Vue.use(VueAMap)
VueAMap.initAMapApiLoader({
  key: '517c01e3260d8c8128723ec438cb1834',
  plugin: [
    'AMap.Geolocation',
    'AMap.Autocomplete',
    'AMap.PlaceSearch',
    'AMap.ToolBar',
    'AMap.MapType'
  ],
  v: '1.4.10'
})
