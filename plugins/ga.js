import { asyncScript } from '~/utils/utils'
/*
** 只在生成模式的客户端中使用
*/
if (process.BROWSER_BUILD && process.env.NODE_ENV === 'production') {
  /*
  ** Google 统计分析脚本
  */
  ;(function(i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () { // eslint-disable-line
      (i[r].q = i[r].q || []).push(arguments) // eslint-disable-line
    }, i[r].l = 1 * new Date(); a = s.createElement(o), // eslint-disable-line
      (m = s.getElementsByTagName(o)[0])
    a.async = 1
    a.src = g
    m.parentNode.insertBefore(a, m)
  })(
    window,
    document,
    'script',
    'https://www.google-analytics.com/analytics.js',
    'ga'
  )
  /*
  ** 当前页的访问统计
  */
  ga('create', 'UA-124238838-1', 'auto') // eslint-disable-line
}

export default ({ app: { router }, store }) => {
  /* 每次路由变更时进行pv统计 */
  router.afterEach((to, from) => {
    /* 告诉增加一个PV */
    try {
      if (to.fullPath.includes('/my')) return
      asyncScript('https://hm.baidu.com/hm.js?2620e69674d25088f870bef114e8ff89')
      window._hmt = window._hmt || []
      window._hmt.push(['_trackPageview', to.fullPath])
      /*
      ** 告诉 GA 增加一个 PV
      */
      ga('set', 'page', to.fullPath) // eslint-disable-line
      ga('send', 'pageview') // eslint-disable-line
    } catch (e) {}
  })
}
