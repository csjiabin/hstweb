import Mock from 'mockjs'
Mock.mock('/mock/user', 'get', {
  'list|10': [
    {
      'id|+1': 1,
      email: '@email',
      'name|+1': '@name',
      'city|+1': '@city(true)',
      'status|1-5': 1
    }
  ]
})
  .mock('/mock/register', 'post', options => {
    const body = JSON.parse(options.body)
    return Mock.mock({
      status: '@boolean',
      ...body
    })
  })
  .mock('/mock/login', 'post', {
    status: '@boolean',
    address: '@county(true)@name',
    phone: /(13|14|15|17|18|19)[0-9]{9}/,
    concat_phone: /(13|14|15|17|18|19)[0-9]{9}/,
    name: '@name',
    concat_name: '@name'
  })
  .mock('/mock/captcha', 'post', options => {
    const reg = /(13|14|15|17|18|19)[0-9]{9}/g
    const { phone } = JSON.parse(options.body)
    if (!phone || !reg.test(phone)) {
      return { phone, status: false }
    }
    return Mock.mock({
      phone,
      status: true,
      'result|100000-999999': 100000
    })
  })
  .mock('/mock/city', 'post', options => {
    const { lat, lng } = JSON.parse(options.body)
    let data = Mock.mock({
      'list|1-21': [
        {
          'id|+1': 101,
          city: '@city',
          'outlet_count|2-10': 2
        }
      ]
    })
    data.list.forEach(item => {
      item.lat = lat + Math.random() * 2
      item.lng = lng + Math.random() * 2
    })
    return { ...data, count: data.list.length }
  })
  .mock('/mock/address', 'post', options => {
    const { lat, lng, outlet_count } = JSON.parse(options.body)
    let template = {}
    template[`list|${outlet_count}`] = [
      {
        'id|+1': 1,
        county: '@county',
        'list|1-3': [
          {
            'id|100000-999999': 100000,
            name: '@name',
            address: '@county(true)@name',
            concat_phone: /(13|14|15|17|18|19)[0-9]{9}/,
            concat_name: '@name',
            tel: /(\(\d{4,4}\)|\d{4,4}-|\s)?\d{8}/
          }
        ]
      }
    ]
    let data = Mock.mock(template)
    data.list.forEach(item => {
      item.lat = lat + Math.random() * 0.05
      item.lng = lng + Math.random() * 0.05
      item.list.forEach(child => {
        child.lat = lat + Math.random() * 0.05
        child.lng = lng + Math.random() * 0.05
      })
    })
    return { ...data, count: data.list.length }
  })
  .mock('/mock/outlet/receipt', 'post', {
    status: '@boolean'
  })
  .mock('/mock/outlet/list', 'post', {
    'list|10-50': [
      {
        'id|1-999999999000': 1,
        outlet_name: '@name',
        outlet_address: '@county(true)@name',
        outlet_tel: /(\(\d{4,4}\)|\d{4,4}-|\s)?\d{8}/,
        concat_phone: /(13|14|15|17|18|19)[0-9]{9}/,
        concat_name: '@name'
      }
    ]
  })
  .mock('/mock/order/list', 'post', {
    stay: '@integer(0, 10)',
    delivery: '@integer(0, 10)',
    finish: '@integer(0, 10)',
    'list|5-10': [
      {
        'id|100000000000-999999999000': 100000000000,
        receipt_time: '@datetime("yyyy-MM-dd HH:mm:ss")',
        'courier_num|100000000000-999999999000': 100000000000,
        courier_company: '@name',
        sender: '@name',
        sender_company: '@name',
        sender_concat_phone: /(13|14|15|17|18|19)[0-9]{9}/,
        recipient: '@name',
        recipient_concat_phone: /(13|14|15|17|18|19)[0-9]{9}/,
        recipient_idCard: /^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/,
        goods_name: '@name',
        'box_num|1-5': 1,
        'item_num|1-5': 1,
        weiget: '@float(0.1, 50)',
        extent: '@float(0.1, 50)',
        width: '@float(0.1, 50)',
        height: '@float(0.1, 50)',
        value: '@float(10, 100000)',
        salesman: '@name',
        from: '@city',
        from_address: '@county(true)@name',
        to: '@city',
        to_address: '@county(true)@name',
        operations: '@name',
        warehouse: '@name',
        outlet_name: '@name'
      }
    ]
  })
  .mock('/mock/order/detail', 'post', options => {
    const body = JSON.parse(options.body)
    let data = Mock.mock({
      ...body,
      'roadmap|3-10': [
        {
          'id|+1': 1,
          context: '[ @city ] [ @name ] @name',
          time: '@datetime("yyyy-MM-dd HH:mm:ss")'
        }
      ]
    })
    data.roadmap.sort(
      (a, b) => new Date(a.time).getTime() - new Date(b.time).getTime()
    )
    return data
  })
