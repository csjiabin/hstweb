import CaptchaInput from './main.vue'
/* istanbul ignore next */
CaptchaInput.install = function(Vue) {
  Vue.component(CaptchaInput.name, CaptchaInput)
}

export default CaptchaInput
