import Amap from './main.vue'
/* istanbul ignore next */
Amap.install = function(Vue) {
  Vue.component(Amap.name, Amap)
}

export default Amap
