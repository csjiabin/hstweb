import Upload from './main.vue'
Upload.install = function(vue) {
  vue.component(Upload.name, Upload)
}

export default Upload
