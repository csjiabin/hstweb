import SearchBox from './main.vue'
SearchBox.install = function(vue) {
  vue.component(SearchBox.name, SearchBox)
}

export default SearchBox
