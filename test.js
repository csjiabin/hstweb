const express = require('express')
const app = express()
const proxyConfig = require('./proxy.config')
const proxyMiddleware = require('http-proxy-middleware')
const resolve = require('path').resolve
// 反向代理
Object.keys(proxyConfig).forEach(function(context) {
  let options = proxyConfig[context]
  if (typeof options === 'string') {
    options = {
      target: options
    }
  }
  app.use(proxyMiddleware(options.filter || context, options))
})

// handle fallback for HTML5 history API
// app.use(require('connect-history-api-fallback')())

// 设置静态资源路径
console.log('静态资源路径', resolve(__dirname, 'dist'))
app.use(express.static(resolve(__dirname, 'dist')))
app.listen(7881, () => console.log('http://localhost:%s', 7881))
