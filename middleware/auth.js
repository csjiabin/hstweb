export default function({ store, redirect }) {
  if (process.client && !localStorage.getItem('user')) {
    return redirect('/expres-single')
  }
}
