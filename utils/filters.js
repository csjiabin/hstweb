export const privacyPhone = phone => {
  if (!phone) return '-'
  let privacyPhone = phone.replace(/(\d{3})\d{4}(\d{4})/, '$1****$2')
  return privacyPhone
}
// 时间格式化
export const dtime = (datetime, format = 'yyyy-MM-dd') => {
  if (!datetime) return '-'
  let t = new Date(datetime)
  let timeFormat = i => (i < 10 ? '0' : '') + i
  return format.replace(/yyyy|MM|dd|hh|mm|ss/g, a => {
    switch (a) {
      case 'yyyy':
        return timeFormat(t.getFullYear())
      case 'MM':
        return timeFormat(t.getMonth() + 1)
      case 'dd':
        return timeFormat(t.getDate())
      case 'hh':
        return timeFormat(t.getHours())
      case 'mm':
        return timeFormat(t.getMinutes())
      case 'ss':
        return timeFormat(t.getSeconds())
    }
  })
}
// 保留小数
export const toFixed = (val, num = 2) => {
  if (!val) return 0
  return Number(val).toFixed(num)
}

export const filterText = (val, arr, key = 'text', result = '') => {
  let target = arr.filter(item => {
    return String(item.value) === String(val)
  })
  if (target.length > 0) return target[0][key || 'text']

  return result || '-'
}
// 补0
export const PrefixInteger = (num = 0, n = 4) => {
  if (!num) return '-'
  return (Array(n).join(0) + num).slice(-n)
}

export const isEmpty = val => {
  if (val === null || val === '' || val === undefined) return '-'
  return val
}

export const beyondReplace = (val, n = 6) => {
  if (!val) return '-'
  return val.length > n ? val.slice(0, n) + '...' : val
}

export const addrJoin = (item, address) => {
  const { provinceName, cityName, areaName } = item || {}
  return (
    (provinceName || '') + (cityName || '') + (areaName || '') + (address || '')
  )
}
